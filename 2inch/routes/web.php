<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::resource('/', 'ActionItemsController');
Route::post('/uploadImage', 'ActionItemsController@uploadImage');


/*
Route::post('/categories/avatars', function(){

    request()->file('avatar')->store('avatars');

    return back();
});
*/

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
