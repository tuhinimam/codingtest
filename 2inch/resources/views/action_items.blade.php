@extends('layouts.app')


@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-6">
                <h1>Action Items</h1>

                <form method="post" action="/uploadImage" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    @foreach($actionItems as $actionItem)

                        <div class="form-check mt-3{{ $actionItem->completed ? ' disabled' : '' }}">
                            <input class="form-check-input" type="radio" name="actionItem" id="actionItem{{ $actionItem->id }}" value="{{ $actionItem->id }}"{{ $actionItem->completed ? ' disabled' : '' }}>
                            <label class="form-check-label" for="actionItem{{ $actionItem->id }}">
                                {{ $actionItem->completed ? '(Done) ' : '' }}{{ $actionItem->descr }}
                            </label>
                        </div>


                    @endforeach

                    <div class="form-group text-cednter mt-5">
                        <input type="file" name="image">
                    </div>
                    <div class="form-group text-cednter mt-3">
                        <button type="submit">Upload</button>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection
