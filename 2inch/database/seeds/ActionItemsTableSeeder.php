<?php

use Illuminate\Database\Seeder;
use App\ActionItem;

class ActionItemsTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('action_items')->delete();
        $json = File::get("database/data/test_data.json");
        $data = json_decode($json,true);

        foreach($data as $obj){
            ActionItem::create($obj);
        }
    }
}
