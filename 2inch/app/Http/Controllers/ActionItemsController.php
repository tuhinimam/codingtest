<?php

namespace App\Http\Controllers;

use Illuminate\Cache\Events\CacheEvent;
use Illuminate\Http\Request;
use App\ActionItem;

class ActionItemsController extends Controller
{

    public function index()
    {
        // Get all the action items
        $actionItems = ActionItem::all();

        return view('action_items', compact('actionItems'));
    }


    public function uploadImage(Request $request)
    {
        // Check that one of the options has been selected
        if($request->actionItem) {

            // If an action item has been selected then get it's ID
            $id = $request->actionItem;

            // Get the object
            $actionItem = ActionItem::find($id);

            // Check that a file has been selected
            if($file = $request->file('image')) {

                // Want to find and store the file extension so the item can be linked to later if and when wanted
                $ext = $file->guessExtension();

                // Store the image
                request()->file('image')->storeAs('public/categories/' . $id, 'image.' . $ext);

                // Mark the item as done and store the file extension.
                $actionItem->ext = $ext;
                $actionItem->completed = true;

                $actionItem->save();
            }
        }

        return back();
    }

}
